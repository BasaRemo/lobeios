//
//  MusicVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import SABlurImageView
import BLKFlexibleHeightBar

//Create the delegate protocol to add a mediaItem to a lobeList
protocol LobeListDelegate {
    func addMusicItemToLobeList(musicItem: SongItem)
    func removeMusicItemFromLobeList(musicItem: SongItem)
    func lobeListContains(musicItem: SongItem) -> Bool

}

class MusicVC: UIViewController {

    @IBOutlet var headerView: SABlurImageView!
    @IBOutlet weak var tableView: UITableView!
    var mediaItems = [MPMediaItem]()
    var collection: MPMediaItemCollection?
    
    var lobeListDelegate: LobeListDelegate?
    var delegateSplitter: BLKDelegateSplitter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        Streaming test
        let streamingPlayer:AudioPlayer = AudioPlayer.defaultPlayer()
        
        streamingPlayer.addSoundFromFile("wiz1.mp3")
        streamingPlayer.addSoundFromFile("wiz2.mp3")
        streamingPlayer.addSoundFromFile("ofu1.mp3")
        streamingPlayer.addSoundFromFile("ofu2.mp3")
        streamingPlayer.addSoundFromFile("ofu3.mp3")

//        streamingPlayer.playQueue()
        
//        End Streaming TEST
        if let _ = self.tableView {
            self.tableView.separatorStyle = .None
            mediaItems = MPMediaQuery.songsQuery().items!
            collection = MPMediaItemCollection(items: mediaItems )
//            AUDIO_OUT_MANAGER.player.setQueueWithItemCollection(collection!)
            AUDIO_OUT_MANAGER.observeMusicChange(self)
//            searchItunesFor("Beatles")
             self.applyBlurEffect()
//            tableView.backgroundView = UIImageView(image: UIImage(named: "success-baby"))
        }
        showBLKMenuView()
    }

    func applyBlurEffect(){
        headerView.configrationForBlurAnimation()
            headerView.addBlurEffect(18, times: 1)
    //    headerView.startBlurAnimation(duration: 0.3)
    //    headerView.configrationForBlurAnimation(100)
    //    headerView?.blur(1)
    }
    
    func showBLKMenuView(){
        
        let bar = BLKFlexibleHeightBar(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 60))
        
        bar.minimumBarHeight = 0.0
        bar.backgroundColor = UIColor.blackColor()
        bar.alpha = 0.9
        
        bar.behaviorDefiner = FacebookStyleBarBehaviorDefiner()
        self.tableView.delegate = bar.behaviorDefiner as? UITableViewDelegate
        
        bar.behaviorDefiner.addSnappingPositionProgress(0.0, forProgressRangeStart:0.0, end:0.5)
        bar.behaviorDefiner.addSnappingPositionProgress(1.0, forProgressRangeStart:0.5, end:1.0)
        
        bar.behaviorDefiner.snappingEnabled = false
        //            bar.behaviorDefiner.elasticMaximumHeightAtTop = true
        
        let button = UIButton(frame: CGRectMake(10, 10, 80, 40))
        button.setTitle("Save", forState: UIControlState.Normal)
        button.titleLabel?.textColor = UIColor.whiteColor()
        button.layer.cornerRadius = 10
        button.layer.borderColor = UIColor.whiteColor().CGColor
        button.layer.borderWidth = 1
        button.addTarget(self, action: Selector("tapped:"), forControlEvents: .TouchUpInside)
//        bar.addSubview(button)
        
        //            self.tableView.contentInset = UIEdgeInsetsMake(bar.maximumBarHeight, 0.0, 0.0, 0.0)
        
        // Configure a separate UITableViewDelegate and UIScrollViewDelegate (optional)
        self.delegateSplitter = BLKDelegateSplitter(firstDelegate: bar.behaviorDefiner, secondDelegate: self)
        self.tableView.delegate = self.delegateSplitter
        
        self.view.addSubview(bar)
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        // if change reload data
        self.tableView.reloadData()
    }
    
    func actOnMusicChange() {
        self.tableView.reloadData()
        print("Music")
    }
    
    func searchItunesFor(searchTerm: String) {
        // The iTunes API wants multiple terms separated by + symbols, so replace spaces with + signs
        let itunesSearchTerm = searchTerm.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
        
        // Now escape anything else that isn't URL-friendly
        if let escapedSearchTerm = itunesSearchTerm.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding) {
            let urlPath = "http://itunes.apple.com/search?term=\(escapedSearchTerm)&media=software"
            let url = NSURL(string: urlPath)
            
            Alamofire.request(.GET, url!).validate().responseJSON { response in
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
//                        print("JSON: \(json)")
                        var results = json["results"]
                        print("JSON: \(results)")
                    }
                case .Failure(let error):
                    print(error)
                }
            }
        }
    }
    

}



extension MusicVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MusicCell", forIndexPath: indexPath) as! MusicCell
        
        let rowItem:MPMediaItem = mediaItems[indexPath.row]
        cell.songItem.music = rowItem
        
        cell.titleLabel?.text = rowItem.valueForProperty(MPMediaItemPropertyTitle) as? String
        cell.artistLabel?.text = rowItem.valueForProperty(MPMediaItemPropertyArtist) as? String
        cell.lobeLIstButton.selected = lobeListDelegate!.lobeListContains(cell.songItem)
        
        cell.onAddToLobeListBtnTapped = { [unowned self] (MusicCell) -> Void in
            
            if (self.lobeListDelegate!.lobeListContains(cell.songItem)){
                self.lobeListDelegate?.removeMusicItemFromLobeList(cell.songItem)
            }else{
                 self.lobeListDelegate?.addMusicItemToLobeList(cell.songItem)
            }
            
        }
        
        return cell
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
//        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
//        UIView.animateWithDuration(0.25, animations: {
//            cell.layer.transform = CATransform3DMakeScale(1,1,1)
//        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaItems.count ?? 0
    }
    
    
}

extension MusicVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //Code to play the music
        AUDIO_OUT_MANAGER.player.setQueueWithItemCollection(collection!)
        AUDIO_OUT_MANAGER.player.nowPlayingItem = collection!.items[indexPath.row]
        AUDIO_OUT_MANAGER.player.play()
        
        //Code to add mediaItem to lobeList
//        lobeListDelegate?.addMusicItemToLobeList(collection!.items[indexPath.row])
        

    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
}