//
//  ContainerVC.swift
//  Lobe
//
//  Created by Professional on 2015-12-28.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import MXSegmentedPager

class ContainerVC: MXSegmentedPagerController {

    var headerView:UIView!
    @IBOutlet var backgroundImage:UIImageView!
    @IBOutlet var countryFlag: UIImageView!
    @IBOutlet var businessName: UILabel!
    @IBOutlet var busineDescription: UILabel!
    @IBOutlet var countryName: UILabel!
    @IBOutlet var closeNowLabel: UILabel!
    var playlistVC: PlaylistVC?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addHeader()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func addHeader(){
        
        // Parallax Header
        headerView = UIView(frame: CGRectMake( 0, 0, self.view.frame.size.width, 220))
        headerView.backgroundColor = UIColor.brownColor()
        headerView.alpha = 1
        
        addHeaderItems()
        self.segmentedPager.backgroundColor = UIColor.clearColor()
//        self.segmentedPager.backgroundColor = UIColor(patternImage: UIImage(named: "success-baby")!)
        
        self.segmentedPager.parallaxHeader.view = headerView
        self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.Fill
        self.segmentedPager.parallaxHeader.height = 395;
        self.segmentedPager.parallaxHeader.minimumHeight = 30;
        
//        var likeView = UIView(frame: CGRectMake( self.segmentedPager.segmentedControl.frame.origin.x + 90 , -30, 150, 50))
//        likeView.backgroundColor = UIColor.redColor()
//        likeView.layer.cornerRadius = 25
        
        let button = UIButton(frame: CGRectMake( self.segmentedPager.segmentedControl.frame.origin.x + 90 , -27, 150, 50))
        button.setTitle("Resume", forState: UIControlState.Normal)
        button.backgroundColor = UIColor.orangeColor()
        button.titleLabel?.textColor = UIColor.whiteColor()
        button.layer.cornerRadius = 26
        button.addTarget(self, action: Selector("tapped:"), forControlEvents: .TouchUpInside)
        
        // Segmented Control customization
        //            self.segmentedPager.backgroundColor = UIColor.clearColor()
        self.segmentedPager.segmentedControl.backgroundColor = UIColor.clearColor()
        self.segmentedPager.segmentedControl.addSubview(button)
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        self.segmentedPager.segmentedControl.backgroundColor = UIColor.blackColor()
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(), NSBackgroundColorAttributeName: UIColor.clearColor(), NSFontAttributeName: UIFont.systemFontOfSize(20)]
        self.segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : UIColor.clearColor()]
        self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe
        self.segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.clearColor()
    }
    
    func addHeaderItems(){
        
        //Init headers items
        businessName = UILabel(frame: CGRectMake( 110, 68, 300, 34))
        busineDescription = UILabel(frame: CGRectMake( 120, 97, 300, 21))
        countryFlag = UIImageView(frame: CGRectMake( 80, 150, 150, 150))
//        countryName = UILabel(frame: CGRectMake( 73, 121, 300, 21))
//        closeNowLabel = UILabel(frame: CGRectMake( 20, 300, 100, 21))
//        backgroundImage = UIImageView(frame: CGRectMake( 0, 0, self.view.frame.size.width, 220))
        
        //Set Raw Data
        countryFlag.image = UIImage(named: "success-baby")
        businessName.text = "In Tha Club"
        busineDescription.text = "50 Cent"
//        closeNowLabel.text = "CLOSED NOW"
//        backgroundImage.image = UIImage(named: "success-baby")
//        countryName.text = "Dem. Republic of Congo"
        
        //Set Raw Data
        busineDescription.font = UIFont.systemFontOfSize(14)
        
        //Label Colors
        businessName.textColor = LABEL_COLOR
        busineDescription.textColor = LABEL_COLOR
//        countryName.textColor = LABEL_COLOR
//        closeNowLabel.textColor = LABEL_COLOR
        
        // Had item to headers
        headerView.addSubview(businessName)
        headerView.addSubview(busineDescription)
        headerView.addSubview(countryFlag)
//        headerView.addSubview(backgroundImage)
//        headerView.addSubview(countryName)
//        headerView.addSubview(closeNowLabel)

    }
    func tapped(sender: UIButton) {
        print("button")
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if let vc = segue.destinationViewController as? ProfileVC
//            where segue.identifier == "profilSegue" {
//                
//                self.embeddedProfilVC = vc
//                self.embeddedProfilVC.headerView = header
//        }
//    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? PlaylistVC
            where segue.identifier == "mx_page_0" {
                print("here")
                self.playlistVC = vc
        }
    }

}

extension ContainerVC{
    
    override func segmentedPager(segmentedPager: MXSegmentedPager, titleForSectionAtIndex index: Int) -> String {
        return ["Informations"][index];
    }
    
    override func segmentedPager(segmentedPager: MXSegmentedPager, didScrollWithParallaxHeader parallaxHeader: MXParallaxHeader) {
//        NSLog("progress %li", parallaxHeader.progress)
    }
    override func segmentedPager(segmentedPager: MXSegmentedPager, segueIdentifierForPageAtIndex index: Int) -> String {
        print("segue")
        return "mx_page_0"
    }
}
