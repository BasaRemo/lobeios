//
//  AlbumVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire

class AlbumVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var mediaItems = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let _ = self.tableView {
            self.tableView.separatorStyle = .None
            mediaItems = MPMediaQuery.albumsQuery().items!
        }

    }

}

extension AlbumVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AlbumCell", forIndexPath: indexPath) as! AlbumCell
        
        let rowItem:MPMediaItem = mediaItems[indexPath.row] as! MPMediaItem
        
        cell.albumNameLabel?.text = rowItem.valueForProperty(MPMediaItemPropertyAlbumTitle) as? String
        cell.songsNumberLabel?.text = rowItem.valueForProperty(MPMediaItemPropertyAlbumTrackCount) as? String
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaItems.count ?? 0
    }
    
}

extension AlbumVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    
}
