//
//  PlaylistVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import RealmSwift
import BLKFlexibleHeightBar


class PlaylistVC: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var popoverView: UIViewController!
    var mediaItems = [MPMediaItem]()
    var collection: MPMediaItemCollection?
    var sectionTitle: String = ""
    var cellEditable = false
    var realm: Realm!
//    var bar:BLKFlexibleHeightBar?
    var delegateSplitter: BLKDelegateSplitter?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let _ = self.tableView {
            self.tableView.separatorStyle = .None
            self.tableView.setEditing(true, animated: false)
            
            AUDIO_OUT_MANAGER.observeMusicChange(self)
        }
        sectionTitle = "New Lobe List"
        realm = try! Realm()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "alertToSave", name:"SaveNotification", object: nil)
        //deletePlist   CellMovedNotification
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateAfterDelete", name:"deletePlist", object: nil)
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
//            showBLKMenuView()
    }
    
    func actOnMusicChange() {
        self.tableView.reloadData()
        print("Playlist")
    }
    func updateAfterDelete(){
        sectionTitle = "New Lobe List"
        self.tableView.reloadData() 
    }
    func alertToSave(){
        
        let alertController = UIAlertController(title: "Save Playlist", message:
            "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "New Playlist Name"
        }
        alertController.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            let textField = alertController.textFields![0] as UITextField
            let newPlaylist = UserPlaylist()
            
            newPlaylist.idPlaylist_ = NSUUID().UUIDString
            newPlaylist.playlistName_ = textField.text!
            
            for elem in self.mediaItems{
                let newTrack = UserTrack()
                newTrack.idTrack_ = ((elem.valueForProperty(MPMediaItemPropertyPersistentID))?.stringValue)!
                newTrack.trackName_ = (elem.valueForProperty(MPMediaItemPropertyTitle) as? String
                    )!
                newPlaylist.tracks_.append(newTrack)
                try! self.realm.write({ () -> Void in
                    self.realm.add(newTrack)
                })
                
            }
            
            try! self.realm.write({ () -> Void in
                self.realm.add(newPlaylist)
            })
            self.sectionTitle = textField.text!
            self.tableView.reloadData() 
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alertController, animated: true){
            
        }
        print("save case")
        
    }
    
    // Mark - Custom Menu Function
    func showBLKMenuView(){

        let bar = BLKFlexibleHeightBar(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 60))
            
        bar.minimumBarHeight = 0.0
        bar.backgroundColor = UIColor.blackColor()
        bar.alpha = 0.9
        
        bar.behaviorDefiner = FacebookStyleBarBehaviorDefiner()
        self.tableView.delegate = bar.behaviorDefiner as? UITableViewDelegate
        
        bar.behaviorDefiner.addSnappingPositionProgress(0.0, forProgressRangeStart:0.0, end:0.5)
        bar.behaviorDefiner.addSnappingPositionProgress(1.0, forProgressRangeStart:0.5, end:1.0)
        
        bar.behaviorDefiner.snappingEnabled = false
//            bar.behaviorDefiner.elasticMaximumHeightAtTop = true
        
        let button = UIButton(frame: CGRectMake(10, 10, 80, 40))
        button.setTitle("Save", forState: UIControlState.Normal)
        button.titleLabel?.textColor = UIColor.whiteColor()
//            button.layer.cornerRadius = 10
//            button.layer.borderColor = UIColor.whiteColor().CGColor
//            button.layer.borderWidth = 1
//            button.addTarget(self, action: Selector("tapped:"), forControlEvents: .TouchUpInside)
//            bar.addSubview(button)
        
//            self.tableView.contentInset = UIEdgeInsetsMake(bar.maximumBarHeight, 0.0, 0.0, 0.0)
        
        // Configure a separate UITableViewDelegate and UIScrollViewDelegate (optional)
        self.delegateSplitter = BLKDelegateSplitter(firstDelegate: bar.behaviorDefiner, secondDelegate: self)
        self.tableView.delegate = self.delegateSplitter
        
        self.view.addSubview(bar)
            
    }
    
    func tapped(sender: UIButton) {
        print("save")
    }
    
}

extension PlaylistVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PlaylistCell", forIndexPath: indexPath) as! PlaylistCell
        
        let rowItem:MPMediaItem = mediaItems[indexPath.row] 
        
        cell.backgroundColor = UIColor.clearColor()
        cell.titleLabel?.text = rowItem.valueForProperty(MPMediaItemPropertyTitle) as? String
        cell.artistLabel?.text = rowItem.valueForProperty(MPMediaItemPropertyArtist) as? String
        cell.movingCellsButton.tag = indexPath.row
        cell.optionListButton.tag = indexPath.row
        cell.optionListButton.addTarget(self, action: "showOptionList:", forControlEvents: .TouchUpInside)
        
        return cell
    }
    
    func showOptionList(sender: UIButton){
        print("\(sender.tag)")
        
        //        let menuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OptionViewCellVC") as! OptionViewCellVC
        //        let popoverV = UIPopoverController(contentViewController:menuViewController) as? UIPopoverController
        //        popoverV!.delegate = self
        //        popoverV!.presentPopoverFromRect(sender.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Down, animated: true)
        //        
        
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaItems.count ?? 0
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitle
    }
    
}

extension PlaylistVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //        AUDIO_OUT_MANAGER.player.setQueueWithItemCollection(collection!)
        //        AUDIO_OUT_MANAGER.player.nowPlayingItem = collection!.items[indexPath.row]
        //        AUDIO_OUT_MANAGER.player.play()
        
        if let _ = self.tableView {
            //mediaItems.removeAtIndex(indexPath.row)
            if (mediaItems.count > 0){
                collection = MPMediaItemCollection(items: mediaItems )
                //                print("\(collection?.count) === \(mediaItems.count)")
                //                AUDIO_OUT_MANAGER.player.nowPlayingItem = mediaItems[indexPath.row]
                //                
                //                AUDIO_OUT_MANAGER.player.play()
            }
            self.tableView.reloadData()
        }
        AUDIO_OUT_MANAGER.notifyMusicChange()
        self.alertToSave()
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    //MARK: - Drag & drop native methode
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.None
    }
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        print(destinationIndexPath)
    }
}

extension PlaylistVC:LobeListDelegate{
    
    func addMusicItemToLobeList(musicItem: SongItem){
        
        if let _ = self.tableView {
            //        mediaItems.insert(musicItem, atIndex: 0)
            mediaItems.append(musicItem.music!)
            collection = MPMediaItemCollection(items: mediaItems )
            self.tableView.reloadData()
        }
        
    }
    
    func removeMusicItemFromLobeList(musicItem: SongItem){
        if let _ = self.tableView {
            mediaItems.removeAtIndex(mediaItems.indexOf(musicItem.music!)!)
            if (mediaItems.count > 0){
                collection = MPMediaItemCollection(items: mediaItems )
            }
            self.tableView.reloadData()
        }
    }
    
    func lobeListContains(musicItem: SongItem) -> Bool {
        
        return mediaItems.contains(musicItem.music!)
    }
    
}



