//
//  MainVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import PagingMenuController
import VYPlayIndicator
import ZFDragableModalTransition
import AVFoundation
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import SABlurImageView



let presentPopUpTransition = PresentPopUpTransition()
let dismissPopUpTransition = DismissPopUpTransition()
let musicNotificationKey = "musicChangeNotification"
//let player = MPMusicPlayerController.applicationMusicPlayer()

class MainVC: UIViewController {
    
    @IBOutlet var playIndicatorView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var playerViewBackground: SABlurImageView!
    var animator:ZFModalTransitionAnimator!
    
    var isPlaying = false
    var timer:NSTimer!
    var webViewVC:UIViewController?
    var viewControllers: [UIViewController]=[]
    var pagingMenuController: PagingMenuController?
    var playlistVC: PlaylistVC? = nil
    var musicVC: MusicVC? = nil
    var containerVC:ContainerVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        musicVC = self.storyboard?.instantiateViewControllerWithIdentifier("MusicVC") as? MusicVC
        playlistVC = self.storyboard?.instantiateViewControllerWithIdentifier("PlaylistVC") as? PlaylistVC
        containerVC = self.storyboard?.instantiateViewControllerWithIdentifier("ContainerVC") as? ContainerVC
//        playlistVC = containerVC.
        musicVC!.lobeListDelegate = playlistVC
        musicVC!.title = "Library"
        playlistVC!.title = "Lobe List"
//
        viewControllers = [musicVC!, containerVC!]
        
        let options = PagingMenuOptions()
        options.menuHeight = 0
        options.font = UIFont.systemFontOfSize(11)
        options.selectedFont = UIFont.systemFontOfSize(11)
        options.menuDisplayMode = .SegmentedControl
        options.backgroundColor = UIColor.blackColor()
        options.selectedBackgroundColor = UIColor.blackColor()
        options.selectedTextColor = UIColor.whiteColor()
        
        pagingMenuController = self.childViewControllers.first as? PagingMenuController
        pagingMenuController!.delegate = self
        pagingMenuController!.setup(viewControllers: viewControllers, options: options)
        
        // Playing music indicator
        let playIndicator:VYPlayIndicator = VYPlayIndicator()
        playIndicator.frame = self.playIndicatorView.bounds;
        self.playIndicatorView.layer.addSublayer(playIndicator)
        playIndicator.animatePlayback()
        
        webViewVC = self.storyboard?.instantiateViewControllerWithIdentifier("LobeWebVC") as? LobeWebVC
        
        AUDIO_OUT_MANAGER.observeMusicChange(self)
        //        applyBlurEffect()
        blurImage()
        self.containerView.backgroundColor = UIColor(patternImage: UIImage(named: "success-baby")!)
        definesPresentationContext = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "mainUpdateAfterDelete", name:"deletePlist", object: nil)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
                print("la")
                musicVC!.lobeListDelegate = containerVC?.playlistVC!
        
    }
    
    func actOnMusicChange() {
        print("MAIN")
    }
    func applyBlurEffect(){
        playerViewBackground.configrationForBlurAnimation()
        playerViewBackground.addBlurEffect(40, times: 1)
    }
    
    func blurImage() {
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = playerViewBackground.bounds
        playerViewBackground.addSubview(blurView)
//        self.view.insertSubview(blurView, belowSubview: self.imageView)
    }
    
    @IBAction func playOrPauseMusic(sender: AnyObject) {
        if isPlaying {
            AUDIO_OUT_MANAGER.player.pause()
            isPlaying = false
        } else {
            AUDIO_OUT_MANAGER.player.play()
            isPlaying = true
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "updateTime", userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func backButton(sender: AnyObject) {
        AUDIO_OUT_MANAGER.player.skipToBeginning()
    }
    @IBAction func forwardButton(sender: UIButton) {
        AUDIO_OUT_MANAGER.player.skipToNextItem()
    }
    @IBAction func stopMusic(sender: AnyObject) {
        AUDIO_OUT_MANAGER.player.stop()
        //        player.nowPlayingItem = 0
        isPlaying = false
    }
    
    func updateTime() {
        //        var currentTime = Int(player.currentTime)
        //        var minutes = currentTime/60
        //        var seconds = currentTime - minutes * 60
        
        //        playedTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
    }
    
    @IBAction func browseMenu(sender: UIButton) {
        
        //    self.applyBlurEffect()
        if pagingMenuController?.currentPage == 0{
            
            let modalVC = self.storyboard?.instantiateViewControllerWithIdentifier("BrowseMenuVC") as? BrowseMenuVC
            
            self.animator = ZFModalTransitionAnimator(modalViewController: modalVC)
            self.animator.dragable = true
            self.animator.bounces = false
            self.animator.behindViewAlpha = 0.5
            self.animator.behindViewScale = 1.0
            self.animator.transitionDuration = 0.7
            self.animator.direction = ZFModalTransitonDirection.Bottom
            //        self.animator.setContentScrollView(modalVC!.tableView)
            
            modalVC!.transitioningDelegate = self.animator
            modalVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
            
            presentViewController(modalVC!, animated: true, completion: nil)
            
        }else{
            
        }
    }
    @IBAction func showLobeSocial(sender: UIButton) {
        
        //    self.applyBlurEffect()
        
        self.animator = ZFModalTransitionAnimator(modalViewController: webViewVC)
        self.animator.dragable = true
        self.animator.bounces = false
        self.animator.behindViewAlpha = 0.5
        self.animator.behindViewScale = 1.0
        self.animator.transitionDuration = 0.7
        self.animator.direction = ZFModalTransitonDirection.Bottom
        //                self.animator.setContentScrollView(webViewVC!.webView)
        
        webViewVC!.transitioningDelegate = self.animator
        webViewVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        presentViewController(webViewVC!, animated: true, completion: nil)
    }
    func mainUpdateAfterDelete(){
        self.showUserPlaylist()
    }
    func showUserPlaylist(){
        if let UserPlaylistsVC = storyboard!.instantiateViewControllerWithIdentifier("UserPlaylistsVC") as? UserPlaylistsVC {
            UserPlaylistsVC.transitioningDelegate = self
            presentViewController(UserPlaylistsVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func showMyPlaylists(sender: UIButton) {
        self.showUserPlaylist()
    }
}

//Mark - Custom modal Transitions animation
extension MainVC:UIViewControllerTransitioningDelegate {
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presentPopUpTransition
    }
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dismissPopUpTransition
    }
}



// MARK: - PagingMenuControllerDelegate

extension MainVC:PagingMenuControllerDelegate{
    
    func willMoveToMenuPage(page: Int) {
    }
    
    func didMoveToMenuPage(page: Int) {
    }
}

extension String {
    func insert(string:String,ind:Int) -> String {
        return  String(self.characters.prefix(ind)) + string + String(self.characters.suffix(self.characters.count-ind))
    }
}


extension MainVC:UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PlaylidtOption"{
            let optionListView:OptionsTableViewVC = segue.destinationViewController as! OptionsTableViewVC
            if playlistVC?.mediaItems.count != 0{
                optionListView.dataArray.append("Clean / new LobeList")
            }
            optionListView.popoverPresentationController?.delegate=self
            optionListView.delegate = self
            
        }
    }
}


extension MainVC:OptionListDelegate{
    func selectedCell(index: Int) {
        //self.selectedIndex = index
        //        print("\(index) =========== \(playlistVC?.mediaItems.count)")
        switch index{
        case 0:
            self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
            self.showUserPlaylist()
            break
        case 1:
            //            PlaylistVC().alertToSave()
            self.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
            NSNotificationCenter.defaultCenter().postNotificationName("SaveNotification", object: nil)
            
            break
        case 2:
            playlistVC?.mediaItems.removeAll()
            playlistVC?.tableView.reloadData()
            musicVC?.tableView.reloadData()
            break
        default:
            break
        }
        
        
    }
}


