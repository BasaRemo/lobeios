//
//  OptionsTableView.swift
//  Lobe
//
//  Created by Nabil Tahri on 15-12-24.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import UIKit

protocol OptionListDelegate{
    func selectedCell(index: Int)
}

class OptionsTableViewVC: UITableViewController {
    var dataArray=["Open existing playlist...", "Save"]
    var indexSelected: Int = 0
    var delegate:OptionListDelegate? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (delegate != nil){
            let i = indexPath.row as Int
            delegate!.selectedCell(i)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("OptionsCell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel!.text = dataArray[indexPath.row]
        return cell
    }
    
    
}
