//
//  UserPlaylistsVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import MediaPlayer
import RealmSwift

class UserPlaylistsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var businessArray = MPMediaQuery.playlistsQuery().collections
    
    var plist: NSMutableArray = []
    var numbOfPlistSongs: NSMutableArray = []
    var realm: Realm!
    
    //NSSortDescriptor(key: "annotationLocation", ascending: true)
    //    ["Bob Marley", "Pink Floyd", "Gun n Rose", "Sean Kingston","Bob marley and","Bob Marley", "Pink Floyd", "Gun n Rose", "Sean Kingston","Bob marley and"]
    //    var songNumbers = ["20", "32", "15", "20","12", "13", "12", "14","20", "21"]
    //NSSortDescriptor *titleSorter= [[NSSortDescriptor alloc] initWithKey:@"annotationLocation" ascending:YES];
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        let savedPlaylists = realm.objects(UserPlaylist)
        //        print("\(savedPlaylists.count)")
        for pls in savedPlaylists{
            plist.addObject(pls.playlistName_)
            numbOfPlistSongs.addObject(pls.tracks_.count)
        }
        
        for pls in businessArray!{
            let plss = pls as! MPMediaPlaylist
            let plsName = plss.valueForProperty(MPMediaPlaylistPropertyName) as! String
            plist.addObject(plsName)
            numbOfPlistSongs.addObject(plss.items.count)
        }
    }
    
    @IBAction func closeView(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

extension UserPlaylistsVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserPlaylistsCell", forIndexPath: indexPath) as! UserPlaylistsCell
        
        //        let playlist: MPMediaPlaylist = businessArray![indexPath.row] as! MPMediaPlaylist
        cell.playlistNameLabel?.text = String(plist.objectAtIndex(indexPath.row))
        //playlist.valueForProperty(MPMediaPlaylistPropertyName) as? String
        
        cell.songsNumberLabel.text = String(numbOfPlistSongs.objectAtIndex(indexPath.row))
        //String(playlist.items.count)
        
        cell.deletePlistButton.tag = indexPath.row
        cell.deletePlistButton.addTarget(self, action: "deletePlist:", forControlEvents: .TouchUpInside)
        
        return cell
    }
    @IBAction func deletePlist(sender: UIButton){
        
        let savedPlaylists = realm.objects(UserPlaylist).filter("playlistName_='\(plist[sender.tag] as! String)'")
        
        if savedPlaylists.count > 0 {
            for elem in savedPlaylists{
                try! realm.write {
                    realm.delete(elem)
                }
                print("Playlist Deleted")
                self.dismissViewControllerAnimated(false, completion: nil)
                NSNotificationCenter.defaultCenter().postNotificationName("deletePlist", object: nil)
            }
        }else{
            let alert = UIAlertController(title: "Alert", message: "This Playlist must be deleted from iTunes", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numbOfPlistSongs.count ?? 0 //businessArray!.count ?? 0
    }
    
}

extension UserPlaylistsVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    
}