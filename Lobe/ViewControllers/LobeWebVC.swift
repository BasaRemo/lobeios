//
//  LobeWebVC.swift
//  Lobe
//
//  Created by Professional on 2015-12-08.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON

class LobeWebVC: UIViewController {

    var webView: WKWebView?
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func loadView() {
        super.loadView()
        
        let contentController = WKUserContentController();
//        let userScript = WKUserScript(
//            source: "redHeader()",
//            injectionTime: WKUserScriptInjectionTime.AtDocumentEnd,
//            forMainFrameOnly: true
//        )
//        contentController.addUserScript(userScript)
        
        contentController.addScriptMessageHandler(
            self,
            name: "initiatePlayMusicFromUrl"
        )
        
        contentController.addScriptMessageHandler(
            self,
            name: "notifyJavaForTimeDiffWithFirebase"
        )
        
        contentController.addScriptMessageHandler(
            self,
            name: "testIOSNative"
        )

        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        self.webView = WKWebView(
            frame: self.view.bounds,
            configuration: config
        )
        self.view.addSubview(self.webView!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView!.navigationDelegate = self
 
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        let url = NSURL(string:"http://lobemusic.com/loginEntryPoint.html")
        let req = NSURLRequest(URL:url!)
        self.webView!.loadRequest(req)

    }

}

extension LobeWebVC:WKNavigationDelegate{
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        //Call JAVASCRIPT
        self.webView!.evaluateJavaScript("fromNativeTest()",completionHandler: { ( result, error) -> Void in
            if let result = result{
                print("result: \(result)")
            }
        })
    }
    
}

extension LobeWebVC:WKScriptMessageHandler{
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        print("\(message.name)")
        switch message.name {
            
            case "testIOSNative":
//                print("message body: \(message.body)")
                let json = JSON(message.body)
                for (_,subJson):(String, JSON) in json {
                    //Do something you want
                    print("\(subJson)")
                }
            
            case "initiatePlayMusicFromUrl":
                print("initiatePlayMusicFromUrl body: \(message.body)")
                let json = JSON(message.body)
                let url = json["music_curr_chunk_url"].string
                if !url!.hasPrefix("http://") {
                    print("no prefix exists")
                    url?.insert("http://", ind: 0)
                }
                AUDIO_OUT_MANAGER.playStreamMusic(url!)
            
            case "notifyJavaForTimeDiffWithFirebase":
                print("initiatePlayMusicFromUrl body: \(message.body)")
                print("local timeStamp:\(TIME_STAMP)")
            
            default:
                print("no message body")
        }
    }
    
}
