//
//  PlaylistCell.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit

class PlaylistCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var coverImage: UIImageView!
    @IBOutlet var optionListButton: UIButton!
    
    @IBOutlet var movingCellsButton: UIButton!
    //    @IBOutlet var addToPlaylistButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        if (editing) {
            
            for view in subviews as [UIView] {
                if view.dynamicType.description().rangeOfString("Reorder") != nil {
                    for subview in view.subviews as! [UIImageView] {
                        if subview.isKindOfClass(UIImageView) {
                            
                            var frame = subview.frame
                            frame.size.height = 22
                            frame.size.width = 5
                            subview.frame = frame
                            subview.backgroundColor = UIColor.clearColor()
                            
                            subview.image =  UIImage(named: "drag_2_col_right_morepoints_no_trans")!
                            //                            NSNotificationCenter.defaultCenter().postNotificationName("CellMovedNotification", object: nil)
                        }
                    }
                }
            }
        }
    }
    
}
