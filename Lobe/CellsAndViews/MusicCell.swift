//
//  MusicCell.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit

class MusicCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var coverImage: UIImageView!
    @IBOutlet var lobeLIstButton: UIButton!
    
    var songItem = SongItem()

    //closure
    var onAddToLobeListBtnTapped : ((MusicCell) -> Void)? = nil
    
    @IBAction func addSongToLobeList(sender: UIButton) {
        
        self.lobeLIstButton.selected = !self.lobeLIstButton.selected
        if let onButtonTapped = self.onAddToLobeListBtnTapped {
            onButtonTapped(self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // MARK: SHOULD refresh the music VC when a music is deleted from lobe list ** NOT WORKING **/
//        songItem.onLobeListStatusChange = { (isInLobeList) in
//            print("SETTING CHANGE")
//            self.lobeLIstButton.selected = !self.lobeLIstButton.selected
//            
//        }
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        self.lobeLIstButton.selected = likeList.contains(PFUser.currentUser()!)
    }
    
    

}
