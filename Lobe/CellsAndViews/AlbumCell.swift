//
//  AlbumCell.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit

class AlbumCell: UITableViewCell {

    @IBOutlet var albumNameLabel: UILabel!
    @IBOutlet var songsNumberLabel : UILabel!
//    @IBOutlet var songLabel : UILabel!
    @IBOutlet var albumCoverImage: UIImageView!
    @IBOutlet var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.containerView.layer.cornerRadius = 10.0
//        self.albumCoverImage.layer.cornerRadius = 20.0
        
//        let shadowPath:UIBezierPath  =  UIBezierPath(rect: self.containerView.bounds)
//        self.containerView.layer.masksToBounds = false;
//        self.containerView.layer.shadowColor = UIColor.blackColor().CGColor
//        self.containerView.layer.shadowOffset = CGSizeMake(0.0, 1.0);
//        self.containerView.layer.shadowOpacity = 0.08
//        self.containerView.layer.shadowRadius = 5.0
//        self.containerView.layer.shadowPath = shadowPath.CGPath;
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
