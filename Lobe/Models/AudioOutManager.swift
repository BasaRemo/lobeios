//
//  AudioOutManager.swift
//  Lobe
//
//  Created by Professional on 2015-12-12.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import WebKit

let AUDIO_OUT_MANAGER = AudioOutManager()
var TIME_STAMP: NSTimeInterval {
    return NSDate().timeIntervalSince1970 * 1000
}

class AudioOutManager:NSObject {

    let player = MPMusicPlayerController.applicationMusicPlayer()
    var streamPlayer = AVPlayer()

    func playStreamMusic(url:String) {
        print("music URL: \(url)")
        let playerItem = AVPlayerItem( URL:NSURL( string:url)! )
        streamPlayer = AVPlayer(playerItem:playerItem)
        streamPlayer.rate = 1.0;
        player.play()
        
    }
    
    func observeMusicChange(viewController: UIViewController){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: "actOnMusicChange", name: musicNotificationKey, object: nil)
    }
    
    @IBAction func notifyMusicChange() {
        NSNotificationCenter.defaultCenter().postNotificationName(musicNotificationKey, object: nil)
    }
}

//extension AudioOutManager:WKScriptMessageHandler{
//    
//    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
//        if(message.name == "testIOSNative") {
//            print("\(message.body)")
//            self.configureStreamPlayer(message.body as! String)
//            
//        }
//    }
//    
//}

