//
//  SongItem.swift
//  Lobe
//
//  Created by Professional on 2015-12-14.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer

class SongItem: MPMediaItem {
    
    var onLobeListStatusChange: ((Bool) -> ())?
    var music:MPMediaItem?

}
