//
//  Album.swift
//  Lobe
//
//  Created by Professional on 2015-12-03.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation

struct Album {
    let title: String
    let price: String
    let thumbnailImageURL: String
    let largeImageURL: String
    let itemURL: String
    let artistURL: String
    
    init(name: String, price: String, thumbnailImageURL: String, largeImageURL: String, itemURL: String, artistURL: String) {
        self.title = name
        self.price = price
        self.thumbnailImageURL = thumbnailImageURL
        self.largeImageURL = largeImageURL
        self.itemURL = itemURL
        self.artistURL = artistURL
    }
    
    //    func toJSON() -> String? {
    //        let props = ["Sentence": self.sentence, "lang": lang]
    //        do {
    //            let jsonData = try NSJSONSerialization.dataWithJSONObject(props,
    //                options: .PrettyPrinted)
    //            return String(data: jsonData, encoding: NSUTF8StringEncoding)
    //        } catch let error {
    //            print("error converting to json: \(error)")
    //            return nil
    //        }
    //    }
    
//    static func albumsWithJSON(rlts: NSArray) -> [Album] {
//        // Create an empty array of Albums to append to from this list
//        var albums = [Album]()
//        
//        // Store the rlts in our table data array
//        if rlts.count>0 {
//            
//            // Sometimes iTunes returns a collection, not a track, so we check both for the 'name'
//            for rlt in rlts {
//                
//                var name = rlt["trackName"] as? String
//                if name == nil {
//                    name = rlt["collectionName"] as? String
//                }
//                
//                // Sometimes price comes in as formattedPrice, sometimes as collectionPrice.. and sometimes it's a float instead of a string. Hooray!
//                var price = rlt["formattedPrice"] as? String
//                if price == nil {
//                    price = rlt["collectionPrice"] as? String
//                    if price == nil {
//                        let priceFloat: Float? = rlt["collectionPrice"] as? Float
//                        let nf: NSNumberFormatter = NSNumberFormatter()
//                        nf.maximumFractionDigits = 2
//                        if priceFloat != nil {
//                            price = "$\(nf.stringFromNumber(priceFloat!)!)"
//                        }
//                    }
//                }
//                
//                let thumbnailURL = rlt["artworkUrl60"] as? String ?? ""
//                let imageURL = rlt["artworkUrl100"] as? String ?? ""
//                let artistURL = rlt["artistViewUrl"] as? String ?? ""
//                
//                var itemURL = rlt["collectionViewUrl"] as? String
//                if itemURL == nil {
//                    itemURL = rlt["trackViewUrl"] as? String
//                }
//                
//                let newAlbum = Album(name: name!, price: price!, thumbnailImageURL: thumbnailURL, largeImageURL: imageURL, itemURL: itemURL!, artistURL: artistURL)
//                albums.append(newAlbum)
//            }
//        }
//        return albums
//    }
}